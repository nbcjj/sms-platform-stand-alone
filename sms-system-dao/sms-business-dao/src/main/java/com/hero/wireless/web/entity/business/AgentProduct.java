package com.hero.wireless.web.entity.business;

import com.hero.wireless.web.entity.base.BaseEntity;
import java.util.Date;

public class AgentProduct extends BaseEntity {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_product.Id
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_product.Agent_No
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    private Integer agent_No;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_product.Product_No
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    private Integer product_No;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_product.Description
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    private String description;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_product.Remark
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    private String remark;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_product.Create_User
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    private String create_User;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column agent_product.Create_Date
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    private Date create_Date;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_product.Id
     *
     * @return the value of agent_product.Id
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_product.Id
     *
     * @param id the value for agent_product.Id
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_product.Agent_No
     *
     * @return the value of agent_product.Agent_No
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public Integer getAgent_No() {
        return agent_No;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_product.Agent_No
     *
     * @param agent_No the value for agent_product.Agent_No
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public void setAgent_No(Integer agent_No) {
        this.agent_No = agent_No;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_product.Product_No
     *
     * @return the value of agent_product.Product_No
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public Integer getProduct_No() {
        return product_No;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_product.Product_No
     *
     * @param product_No the value for agent_product.Product_No
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public void setProduct_No(Integer product_No) {
        this.product_No = product_No;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_product.Description
     *
     * @return the value of agent_product.Description
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public String getDescription() {
        return description;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_product.Description
     *
     * @param description the value for agent_product.Description
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_product.Remark
     *
     * @return the value of agent_product.Remark
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_product.Remark
     *
     * @param remark the value for agent_product.Remark
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_product.Create_User
     *
     * @return the value of agent_product.Create_User
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public String getCreate_User() {
        return create_User;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_product.Create_User
     *
     * @param create_User the value for agent_product.Create_User
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public void setCreate_User(String create_User) {
        this.create_User = create_User;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column agent_product.Create_Date
     *
     * @return the value of agent_product.Create_Date
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public Date getCreate_Date() {
        return create_Date;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column agent_product.Create_Date
     *
     * @param create_Date the value for agent_product.Create_Date
     *
     * @mbg.generated Tue Oct 19 10:30:00 CST 2021
     */
    public void setCreate_Date(Date create_Date) {
        this.create_Date = create_Date;
    }
}